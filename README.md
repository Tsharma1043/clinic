*******************************************************************************

Hello 

My name is Tushar Sharma  
1. I have created a public repository in which i have uploaded my Project which is website for a Patient Record.
2. I have taken MIT licensed which is free.
3. I have created a Wiki File that contain the Information about the MVC Framework in ASP.net 


## Overview

This is basic Website that is Created in Asp .net  using Visual Studio which display a patient information and other 
information . In the Backend it is connected to a Database to store the Patient Information and CRUD Operations is applied on it 
and thtough admin access, admin can perform these operation of the Website.
This website works on MVC Framework . To Understand the MVC i have Wiki File so that access all the Information About MVC Framework
and how to apply MVC while creating the Application.

## Minimum Requirements
1. You need to have Visual Studio 2019 Install on your System
2. A SQL Server Management Studio to for the Database Connection 

## Things You have to Download
1. Complete TSPatients folder and save it on your Desktop 
2. Download the Patients.SQl File 

## How to Run this File 

1. Open Visual Studio 2019
2. Go to File and Open Project and search for TSPatient File that you hav downloaded 
3. Open SQL Server Management and Run the Patients.sql file that you have downloaded
4. After that Build the Solution to check there is error in it 
5. Run the File , you will see that your default internet explorer will open up and will show thewebsite  

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

